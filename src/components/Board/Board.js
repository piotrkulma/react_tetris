/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/
import {TILE_SIZE} from '../../Config'


import {getBrickStyle} from '../Utils/BoardUtils'

import './Board.css'

const board = (props) => {
    let gameOverText = null

    if (props.gameOver === true) {
        gameOverText = (<text className="GameOver" x="15" y="150" fill="black">GAME IS OVER</text>)
    }

    return <svg
        className="SvgBoard"
        width={props.board.length * TILE_SIZE}
        height={props.board[0].length * TILE_SIZE}>
        {
            props.board.map((el, i) => {
                return (el.map((brick, j) => {
                    return (
                        <rect
                            key={i + "_" + j}
                            x={i * TILE_SIZE} y={j * TILE_SIZE}
                            width="10" height="10"
                            style={getBrickStyle(props.board[i][j], props.actualColor)}/>
                    )
                }))
            })
        }
        {gameOverText}
    </svg>
}

export default board