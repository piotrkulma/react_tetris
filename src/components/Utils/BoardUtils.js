/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/

import {SHAPES} from './Shapes'
import {BOARD_WIDTH, BOARD_HEIGHT} from '../../Config'

const EMPTY_BRICK = '0'
const TO_FILL_BRICK = 'x'

const KEYS = {
    KEY_UP: 'ArrowUp',
    KEY_DOWN: 'ArrowDown',
    KEY_LEFT: 'ArrowLeft',
    KEY_RIGHT: 'ArrowRight',
    KEY_SPACE: ' '
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function randomColor() {
    return (getRandomInt(7) + 1).toString();
}

const randomShape = () => {
    return SHAPES[getRandomInt(SHAPES.length)];
}

const randomIndex = () => {
    return getRandomInt(3);
}

const linesToDelete = (board) => {
    let test;
    let lines = [];

    for (let j = 0; j < board[0].length; j++) {
        test = true;
        for (let i = 0; i < board.length; i++) {
            if(!(board[i][j] > 0)) {
                test = false;
                break;
            }
        }

        if(test) {
            lines.push(j)
        }
    }

    return lines
}

const merge = (board, shape, x, y, fill = TO_FILL_BRICK) => {
    for (let i = 0; i < shape.length; i++) {
        for (let j = 0; j < shape[0].length; j++) {
            if (shape[i][j] === TO_FILL_BRICK) {
                board[x + i][y + j] = fill
            }
        }
    }

    return board
}

const isMergeEnable = (board, shape, x, y) => {
    for (let i = 0; i < shape.length; i++) {
        for (let j = 0; j < shape[0].length; j++) {
            if (shape[i][j] === TO_FILL_BRICK) {
                if (!(x + i >= 0 && x + i < board.length && y + j < board[0].length)) {
                    return false
                }

                if ((y + j) >= 0 && (board[x + i][y + j] !== EMPTY_BRICK) &&
                    (board[x + i][y + j] !== TO_FILL_BRICK)) {
                    return false
                }
            }
        }
    }

    return true
}

const clear = (board, shape, x, y) => {
    return merge(board, shape, x, y, EMPTY_BRICK)
}

const getBrickBaseColor = (val) => {
    switch (val) {
        case '1':
            return 'green';
        case '2':
            return 'blue';
        case '3':
            return 'orange';
        case '4':
            return 'khaki';
        case '5':
            return 'magenta';
        case '6':
            return 'wheat';
        case '7':
            return 'honeydew';
        default:
            return 'aquamarine'
    }
}

const getBrickColor = (val, actualColor) => {
    switch (val) {
        case 'x':
            return getBrickColor(actualColor, null);
        default:
            return getBrickBaseColor(val)
    }
}

const getBrickStyle = (val, actualColor) => {
    const style = {
        "fill": getBrickColor(val, actualColor)
    }

    return style
}

const createBoard = (tiles = EMPTY_BRICK) => {
    let board = [];

    for (let i = 0; i < BOARD_WIDTH; i++) {
        board.push(new Array(BOARD_HEIGHT))
        for (let j = 0; j < BOARD_HEIGHT; j++) {
            board[i][j] = tiles
        }
    }

    return board
}

const copyBoard = (board) => {
    let boardCopy = [];

    for (let i = 0; i < board.length; i++) {
        boardCopy.push(new Array(board[0].length))
        for (let j = 0; j < board[0].length; j++) {
            boardCopy[i][j] = board[i][j]
        }
    }

    return boardCopy
}

const changeIndex = (direction, value) => {
    let ret = value + direction;

    if (ret < 0) {
        ret = 3;
    } else if (ret > 3) {
        ret = 0;
    }

    return ret;
}


export {
    merge,
    clear,
    isMergeEnable,
    linesToDelete,
    createBoard,
    copyBoard,
    changeIndex,
    randomColor,
    randomShape,
    randomIndex,
    getBrickStyle,
    KEYS
}