/*eslint-disable no-unused-vars*/
import React from 'react'
import {getBrickStyle} from "../Utils/BoardUtils";
/*eslint-disable no-unused-vars*/

const nextElement = (props) => {
    return <svg
            className="SvgBoard"
            width={props.element.length * 12}
            height={props.element[0].length * 12}>
            {
                props.element.map((el, i) => {
                    return (el.map((brick, j) => {
                        return (
                            <rect
                                key={i + "_" + j}
                                x={i * 12} y={j * 12}
                                width="10" height="10"
                                style={getBrickStyle(props.element[i][j], props.color)}/>
                        )
                    }))
                })
            }
        </svg>
}

export default nextElement