import React, {Component} from 'react'
import './GameLabel.css'

class GameLabel extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if(nextProps.value instanceof Object) {
            if(nextProps.index !== this.props.index) {
                return true
            }

            return false
        }

        if(nextProps.value !== this.props.value) {
            return true
        }

        return false
    }

    render() {
        return (
            <div className="GameLabel">
                <p className="GameLabelParagraph"><b>{this.props.name}</b><br/>{this.props.value}</p>
            </div>)
    }
}

export default GameLabel