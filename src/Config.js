/*eslint-disable no-unused-vars*/
import React from 'react'
/*eslint-disable no-unused-vars*/

const BOARD_WIDTH = 15
const BOARD_HEIGHT = 25
const TILE_SIZE = 12

export {BOARD_WIDTH, BOARD_HEIGHT, TILE_SIZE}