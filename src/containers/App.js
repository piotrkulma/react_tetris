import React, {Component} from 'react';
import Board from '../components/Board/Board'
import NextElement from '../components/NextElement/NextElement'
import GameLabel from '../components/GameLabel/GameLabel.js'

import {
    changeIndex,
    clear,
    createBoard,
    copyBoard,
    isMergeEnable,
    linesToDelete,
    randomColor,
    randomShape,
    randomIndex,
    KEYS,
    merge
} from '../components/Utils/BoardUtils'

import './App.css';

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            board: createBoard(),
            actualShape: randomShape(),
            shapeIndex: randomIndex(),
            shapeColor: randomColor(),
            nextShape: randomShape(),
            nextColor: randomColor(),
            positionX: 5,
            positionY: -3,
            points: 0,
            pointsInLevel: 0,
            tick: 0,
            level: 1,
            gameOver: false,
            queue: []
        }
    }

    moveShape = (board, x, y, index) => {
        if (isMergeEnable(board, this.state.actualShape[index], x, y)) {

            board = clear(board, this.state.actualShape[this.state.shapeIndex], this.state.positionX, this.state.positionY)
            board = merge(board, this.state.actualShape[index], x, y)
            this.setState({board: board, positionX: x, positionY: y, shapeIndex: index})

            return true
        }

        return false;
    }

    deleteLines = () => {
        let toDelete = linesToDelete(this.state.board)

        const lineCount = toDelete.length

        if (lineCount === 0) {
            return
        }

        let boardCopy = copyBoard(this.state.board)
        toDelete.forEach(el => {
            for (let i = 0; i < this.state.board.length; i++) {
                for (let j = 0; (el - j) >= 0; j++) {
                    boardCopy[i][el - j] = this.state.board[i][el - (j + 1)]
                }
            }
        });

        this.setState({board: boardCopy})

        return lineCount
    }

    runLineDelete=() => {
        const lineCount = this.deleteLines()

        if (lineCount > 0) {
            this.setState({pointsInLevel: this.state.pointsInLevel + lineCount})
        }


        if (this.state.pointsInLevel > this.state.level) {
            this.setState((prevState, props) => {
                return {
                    level: prevState.level + 1,
                    points: prevState.points + prevState.pointsInLevel,
                    pointsInLevel: 0}
            })
        }
    }

    runQueue = () => {
        if (this.state.queue.length === 0) {
            return
        }

        let queueCopy = [...this.state.queue]
        let boardCopy = copyBoard(this.state.board)

        let [x, y, index] = [this.state.positionX, this.state.positionY, this.state.shapeIndex]

        queueCopy.forEach((key) => {
            if (key === KEYS.KEY_SPACE) {
                index = changeIndex(-1, this.state.shapeIndex);
            } else if (key === KEYS.KEY_LEFT) {
                x = this.state.positionX - 1;
            } else if (key === KEYS.KEY_RIGHT) {
                x = this.state.positionX + 1;
            } else if (key === KEYS.KEY_DOWN) {
                y = this.state.positionY + 1;
            }

            this.moveShape(boardCopy, x, y, index)
        })

        this.setState({board: boardCopy, queue: []})

    }

    timeTickHandler = () => {
        this.runQueue();

        if (this.state.tick <= (55 - (this.state.level * 5))) {
            this.setState((prevState, props) => {
                return {tick: prevState.tick + 1}
            })
            return
        }

        this.runLineDelete()

        if (this.state.gameOver) {
            return;
        }

        let boardCopy = copyBoard(this.state.board)
        const brickMoved = this.moveShape(boardCopy, this.state.positionX, this.state.positionY + 1, this.state.shapeIndex)
        if (!brickMoved) {
            if (this.state.positionY === -3) {
                this.setState({gameOver: true})
                return;
            }

            boardCopy = merge(boardCopy, this.state.actualShape[this.state.shapeIndex], this.state.positionX, this.state.positionY, this.state.shapeColor)

            this.setState({
                board: boardCopy,
                positionX: 5,
                positionY: -5,
                shapeIndex: randomIndex(),
                actualShape: this.state.nextShape,
                shapeColor: this.state.nextColor,
                nextShape: randomShape(),
                nextColor: randomColor(),
                tick: 0
            })
        } else {
            this.setState({tick: 0})
        }
    }

    onKeyDownHandler = (event) => {
        event.preventDefault();

        if (this.state.gameOver || this.state.positionY <= -3) {
            return;
        }

        if (![KEYS.KEY_SPACE, KEYS.KEY_LEFT, KEYS.KEY_RIGHT, KEYS.KEY_DOWN].includes(event.key)) {
            return;
        }

        this.setState((prevState, props) => {
            return {queue: [...prevState.queue, event.key]}
        })
    }

    componentDidMount() {
        document.addEventListener('keydown', this.onKeyDownHandler);
        this.interval = setInterval(() => this.timeTickHandler(), 10);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.onKeyDownHandler);
        clearInterval(this.interval);
    }

    render() {
        return (
            <div className="App">
                <Board
                    gameOver={this.state.gameOver}
                    actualColor={this.state.shapeColor}
                    board={this.state.board}/>
                <div>
                    <GameLabel name="Score" value={this.state.points + this.state.pointsInLevel}/>
                    <GameLabel name="Level" value={this.state.level}/>
                    <GameLabel name="Next" value={<NextElement color={this.state.nextColor} element={this.state.nextShape[0]}/>} index={this.state.shapeIndex}/>
                </div>
            </div>
        );
    }
}

export default App;
